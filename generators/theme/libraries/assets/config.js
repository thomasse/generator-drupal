/**
 * Vous pouvez surcharger ici la configuration par défaut de chaque tache.
 */
module.exports = {
 "build": {
   "src": "src/",
   "dev": "../dev",
   "prod": "../dist"
 },
//  "copy": {
//    "ext": "*",
//    "src": "copy/**/*",
//    "watch": "copy/**/*",
//    "dest": "./"
//  },
//  "fonts": {
//    "src": "*/fonts/**/*",
//    "dest": "fonts"
//  },
//  "iconfont": {
//    "ext": "svg",
//    "src": "icons/**/*.",
//    "watch": "icons/**/*.",
//    "dest": "fonts/iconfont",
//    "config": {
//      "fontName": "iconfont",
//      "fontHeight": 1001,
//      "normalize": true,
//      "prependUnicode": true,
//      "formats": [
//        "ttf",
//        "eot",
//        "woff",
//        "woff2",
//        "svg"
//      ],
//      "timestamp": 1615968634
//    },
//    "css": {
//      "templatePath": "default",
//      "fontName": "iconfont",
//      "fontPath": "./fonts/iconfont/",
//      "className": "ic",
//      "dest": "scss/iconfont/"
//    }
//  },
//  "images": {
//    "src": "img/**/*",
//    "dest": "img",
//    "watch": "*/img/**/*"
//  },
//  "movies": {
//    "ext": "{png,jpg,jpeg}",
//    "src": "movies/*/**",
//    "watch": "movies/**/*",
//    "dest": "./movies/",
//    "css": {
//      "templatePath": "default",
//      "dest": "scss/movies/"
//    }
//  },
 "refresh": {
   "watch": ["../../templates/*.html.twig", '../../*.theme']
 },
//  "sass": {
//    "src": "*/scss/*.scss",
//    "dest": "",
//    "watch": "*/scss/**/*.scss"
//  },
//  "scripts": {
//    "ext": "{js,ts}",
//    "src": "js/*.",
//    "dest": "",
//    "watch": "js/**/*."
//  },
//  "sprites": {
//    "ext": "*",
//    "src": "sprites/**/*",
//    "watch": "sprites/**/*",
//    "dest": "./",
//    "sprites": {
//      "imgName": "sprite.png",
//      "cssName": "scss/base/_sprite.scss"
//    }
//  }

};
