module.exports = {
  "type": "browser-sync",
  "config": {
    proxy: "<%= projectId %>_server:8080",
    notify: false,
    // Do not open browser on start
    open: false,
  }}
