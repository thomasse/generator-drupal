// Default conf
exports.defaultConfiguration = {
  'toolbar': {
    // Répertoire des sources
    src: 'toolbar-icon/*.svg',
    watch: 'toolbar-icon/*.svg',
    // Répertoire de destination.
    dest: './toolbar-icons/',
    // CSS
    css: {
      templatePath: 'default',
      dest: 'scss/toolbar/'
    }
  }
}
