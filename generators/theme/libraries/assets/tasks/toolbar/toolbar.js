const {watch, src, dest} = require('gulp')
const path = require('path')
const through = require('through2')
const glob = require ('glob')
const consolidate = require('gulp-consolidate')

const messenger = require('bim-gulp/utils/Messenger')
const Reload = require('bim-gulp/utils/ReloadTools')
const bundleTools = require('bim-gulp/utils/BundleTools')
const handleErrors = require('bim-gulp/utils/error')
const conf = require('bim-gulp/utils/Config')
const config = conf.get('toolbar').toolbar

/**
 * Gestion des toolbars icons
 */
function toolbar(done){
    return bundleTools(conf.getSrcPath('*'), done).src(processBundle)
}

/**
* Processus scopé dans un bundle particulier.
*/
function processBundle(source){
    // Récupération de la pattern des points d'entrée
    source = path.join(source, config.src)
    const bundleName = conf.getFileBundleName(source);

    // Ex:
    return src(source)
      .on('error', handleErrors)
      .pipe(dest(conf.getDestPath(path.join(conf.getFileBundleName(source), config.dest))))
      .pipe(through.obj((data, enc, cb) => generateScssTemplate(source, cb)))
}

function generateScssTemplate(source, cb){
  const files = glob.sync(source).map(item =>{
    return {
      id: path.basename(item).split('.')[0],
      url: path.join(config.dest, path.basename(item)),
    }
  });

  const templateOptions = {
    files: files,
    config: config,
  };

  const template = config.css.templatePath === 'default' ? path.join(__dirname, 'template', '_template.scss') : config.css.templatePath

  return src(template)
    .pipe(consolidate('lodash', templateOptions))
    .pipe(dest(path.join(conf.getFileBundlePath(source), config.css.dest)))
    .on('finish', ()=>{
      cb();
    })
}

function onFileChanged(file, data){
    // Pour des raisons de perfs, on ne reconstruit que le bundle concerné par le fichier
    processBundle(conf.getFileBundlePath(file))
}

/**
 * Lance les actions au watch des sass.
 */
function watchProcess() {
    const watcher = watch(conf.getSrcPath(path.join('*', config.watch)))

    watcher.on('change', (file, data) => onFileChanged(file, data))
    watcher.on('add', (file, data) => onFileChanged(file, data))
}

exports.watchProcess = watchProcess
exports["toolbar"] = toolbar

/**
* A décommenter si la tache doit êter exécutée avant le watch.
*/
exports.beforeWatch = exports["toolbar"]
