# Gulp watch

## Watch
Pour être certain d'utiliser les bonnes version d'npm et consorts, il est primordial de
lancer gulp via le container node. Pour cela il existe la commande :
`make node-up`

## Accès sur le réseau public
Le fait de lancer le watch depuis le container empèche browsersync d'être accessible
sur le réseau public. Pour accéder à browsersync depuis le réseau, on doit lancer
un browsersync hors containeur qui proxy le container node et et écoute les modifs de fichier.
C'est notammenet utile pour visualiser les modifications de fichiers sur device mobile.

Pour cela, il faut avoir installer browsersync en global hors container
`npm i -g browserync`

Puis lancer le script sh
`./network-watch.sh`
