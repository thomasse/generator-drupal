const BaseClass = require('../../tools/app/AppGenerator')
const fsCore = require('fs')
const path = require('path')
const fse = require('fs-extra')

module.exports = class extends BaseClass {


    _getDependencies(){
        return [
            'composer',
        ]
    }

    _getQuestions() {
        return [
            {
                type: "input",
                name: "projectId",
                message: "Id du projet",
            },
            {
                type: "input",
                name: "projectName",
                message: "Nom du projet",
            },
            {
                type: "input",
                name: "rootDir",
                message: "Nom du répertoire applicatif",
                default: (options) => {
                    return 'app'
                },
            },
            {
                type: "input",
                name: "themeName",
                message: "Nom du theme",
                default: options => {
                    return (options.projectId || this.options.projectId) + '_theme';
                }
            },
        ]
    }


    async main() {
        console.log("============ Theme ");

        // Récupération des arguments
        this.options = await this.askQuestions(this.options);

        // Crétion du répertoire et copie des templates
        await this._copyTemplate();

        // copie des assets du theme avec task custom, donc avec de l'ejs.
        this._copyAdminAssets();

        // Création des fichier symli
        this._createSymlink();
    }

    /**
     * Copy des templates
     */
    async _copyTemplate() {

        // All dote files.
        try{
            this.fs.copyTpl(
                this.templatePath('./**/*'),
                this.destination(),
                this.options,
            )
        }
        catch (e) {
            console.log(1)
            console.log(e)
        }


        try{
            this.fs.copyTpl(
                this.templatePath('./**/.*'),
                this.destination(),
                this.options,
            )
        }
        catch(e){
            console.log(2)
            console.log(e)
        }



        return this.commitWriteFiles();
    }

    _createSymlink(){
        // front theme
        let dest = this.destination(`${this.options.rootDir}/web/themes/custom/${this.options.themeName}/libraries/assets/node_modules/gulp/bin/gulp.js`)
        fsCore.mkdirSync(path.dirname(dest), { recursive: true });
        fsCore.writeFileSync(dest, '')
        this._symLink(
            dest,
            this.destination(`${this.options.rootDir}/web/themes/custom/${this.options.themeName}/libraries/assets/gulp`)
            );
        fsCore.unlinkSync(dest)

        // admin theme
        dest = this.destination(`${this.options.rootDir}/web/themes/custom/${this.options.projectId}_admin_theme/libraries/assets/node_modules/gulp/bin/gulp.js`)
        fsCore.mkdirSync(path.dirname(dest), { recursive: true });
        fsCore.writeFileSync(dest, '')
        this._symLink(
            dest,
            this.destination(`${this.options.rootDir}/web/themes/custom/${this.options.projectId}_admin_theme/libraries/assets/gulp`)
            );
        fsCore.unlinkSync(dest)
    }

    _copyAdminAssets() {
        fse.copySync(
            this.templatePath('../libraries'),
            this.destination(`${this.options.rootDir}/web/themes/custom/${this.options.projectId}_admin_theme/libraries`)
        )
    }
};
