const BaseClass = require('../../tools/app/AppGenerator')
const {execSync} = require("child_process");

module.exports = class extends BaseClass {

	/**
	 * Liste des paramètres.
	 * @return {{default: (String|*), name: string, type: string, message: string}[]}
	 * @private
	 */
	_getDependencies() {
		return ['git']
	}

	/**
	 * Liste des paramètres.
	 * @return {{default: (String|*), name: string, type: string, message: string}[]}
	 * @private
	 */
	_getQuestions() {
		return [
		]
	}

	async main() {
		console.log("============ Initialisation git");

		// Récupération des arguments
		this.options = await this.askQuestions(this.options);

		// Crétion du répertoire et copie des templates

		return this.spawnCommandSync('make', ['node-up'], {cwd: this.destination()});
	}
};
