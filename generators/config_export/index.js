const Generator = require('yeoman-generator')
const fs = require('fs')
const fse = require('fs-extra')
const path = require('path')
const glob = require('glob')
const yml = require('yamljs')

module.exports = class extends Generator {

    async main() {
        console.log("============ Config export");

        this.options = await this.prompt([
            {
                type: "input",
                name: "projectId",
                message: "Id du projet",
                default: options => {
                    return path.basename(this.destinationPath())
                }
            },
            {
                type: "input",
                name: "projectName",
                message: "Nom du projet",
                default: options => {
                    return options.projectId;
                }
            },
            {
                type: "input",
                name: "rootDir",
                message: "Nom du répertoire applicatif",
                default: (options) => {
                    return 'app'
                },
            },
            {
                type: "input",
                name: "moduleName",
                message: "Nom du module",
                default: options => {
                    return (options.projectId || this.options.projectId) + '_module';
                }
            },
            {
                type: "input",
                name: "themeName",
                message: "Nom du theme",
                default: options => {
                    return (options.projectId || this.options.projectId) + '_theme';
                }
            }
        ])

        // init replacement.
        this.replacement = [];
        Object.keys(this.options).map(key => {
            this.replacement.push({
                key:key,
                value:this.options[key],
            })
        })
        this.replacement.sort((a,b) => { return a.value.length >= b.value.length ? -1: 1 })

        // Prepare repertoire.
        this._prepareRep()

        // Init specific module.
        this._initModule()

        try{
            // Init Themes
            this._initThemes();
        }
        catch(e){
        }


        // Profile.
        this._initProfile();

        // replace tokens.
        this._replaceTokens();
    }

    _prepareRep() {
        this.repName = 'yo_config_export'

        if (fs.existsSync(this.repName)) {
            fse.removeSync(this.repName)
        }

        console.log(`Création du répertoire ${this.repName}`)
        fs.mkdirSync(this.repName);
    }

    /**
     * Création du template de module.
     * @private
     */
    _initModule() {
        [
            this.destinationPath(`${this.options.rootDir}/web/modules/custom/` + this.options.moduleName)
        ].map(item => {
            this._copy(item, this.repName + '/modules')
        })

        // Module Name
        // this._replaceFile(glob.sync(this.repName + '/modules'))
    }

    _initThemes() {
        // Default theme
        [
            this.destinationPath(`${this.options.rootDir}/web/themes/custom/${this.options.projectId}_theme`),
            this.destinationPath(`${this.options.rootDir}/web/themes/custom/${this.options.projectId}_admin_theme`)
        ].map(item => {
            const themeId = path.basename(item)
            this._copy(item, this.repName + '/themes')

            this._copyConf(themeId, this.repName + '/themes/' + themeId + '/config/install');
        })
    }

    _copy(from, to) {
        to += '/' + path.basename(from)
        fse.copySync(from, to, {
            filter: (item) => {
                return item.indexOf('node_modules') < 0
            }
        })
    }

    _copyConf(themeId, destination) {
        const files = glob.sync(this.destinationPath(`${this.options.rootDir}/config/sync/*${ themeId }*`))
            .map(item => {
                this._copy(item, destination)
            })
    }

    _replaceTokens() {
        this._replaceFile(glob.sync(this.destinationPath(this.repName + '/*')))
    }

    _replace(item) {
        this.replacement.map(
            replacem => {
                while( item.indexOf(replacem.value) > -1 ){
                    item = item.replace(replacem.value, `<%= ${replacem.key} %>`)
                }
            }
        )
        return item;
    }

    _replaceFile(files) {
        files.map(file => {
            const dest = path.dirname(file) + '/' + this._replace(path.basename(file))
            const hasToken = dest !== file;

            if (fs.lstatSync(file).isDirectory()) {
                this._replaceFile(glob.sync(file + '/*'))
                if (hasToken) {
                    // fse.copySync(file, dest)
                    fs.renameSync(file, dest)
                }
            } else if (fs.lstatSync(file).isFile()) {
                const content = this._replace(fs.readFileSync(file, 'utf8'))
                fs.writeFileSync(hasToken ? dest : file, content)
                if (hasToken) {
                    fs.unlinkSync(file)
                }
            }
        })
    }

    _initProfile() {
        // Default theme
        [
            this.destinationPath(`${this.options.rootDir}/config/sync/`),
        ].map(item => {
            this._copy(item, this.repName + '/profile')
        })

        // rename
        fs.renameSync(this.repName + '/profile/sync', this.repName + '/profile/install')

        // delete htaccess
        fs.unlinkSync(this.repName + '/profile/install/.htaccess')


        // Init core.extension.
        const extension = yml.load(this.repName+'/profile/install/core.extension.yml');
        fs.writeFileSync(this.repName+'/profile/<%= profileName %>.info.yml', yml.stringify(
            {
                name: '<%= projectName %> profile',
                type: 'profile',
                version: '1.0',
                core: '8.x',
                core_version_requirement: '^8 || ^9',
                dependencies: Object.keys(extension.module),
                themes: Object.keys(extension.theme),
            }, 1000, 2
        ))

        // Suppression des choses en trop.
        fse.removeSync(this.repName+'/profile/install/language')
        fs.unlinkSync(this.repName+'/profile/install/core.extension.yml')

        // Suppression des uuid.
        glob.sync(this.repName+'/profile/install/*.yml').forEach(file =>{
            const data = yml.load(file);
            ['uuid', '_core'].forEach( item => {
                delete( data[item])
            })
            fs.writeFileSync( file, yml.stringify(data, 1000, 2))
        })
    }
}
