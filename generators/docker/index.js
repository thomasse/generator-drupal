const BaseClass = require('../../tools/app/AppGenerator')
const slug = require('../../tools/io/io').slug

module.exports = class extends BaseClass {

	async main() {
		console.log("============ Docker");

		// Prompt if needed.
		this.options = await this.askQuestions(this.options)

		// Tempaltes
		await this._copyTemplates()

		// Install docker
		await this._install()
	}

	/**
	 * Dependencies.
	 * @return {string[]}
	 * @private
	 */
	_getDependencies() {
		return [
			'env'
		]
	}

	/**
	 * Questinos.
	 * @return {{filter: (function(*=): *), name: string, type: string, message: string, required: boolean}[]}
	 * @private
	 */
	_getQuestions() {
		return [
			{
				type: "input",
				name: "projectId",
				message: "ID du projet (nom du répertoire, slug)",
				required: true,
				filter: (v) => {
					return slug(v)
				}
			},
			{
				type: "input",
				name: "rootDir",
				message: "Nom du répertoire applicatif",
				default: (options) => {
					return 'app'
				},
			},
		]
	}

	/**
	 * Copie des fichiers de conf.
	 *
	 * @return {Promise<unknown>}
	 * @private
	 */
	async _copyTemplates() {
		this.fs.copyTpl(
			this.templatePath('./.docker/**/*'),
			this.destination('./.docker'),
			this.options,
		)

		return this.commitWriteFiles();
	}

	/**
	 * Up les conatiners.
	 *
	 * @return {Promise<void>}
	 * @private
	 */
	async _install() {
		this.spawnCommandSync('make', ['build-all'], {cwd:this.destination()})
	}
};
