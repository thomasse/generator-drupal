##
#############################    node    #####################################7

NODE_DIR = '.'

CONNECT_NODE ?= docker exec -ti --user root $(COMPOSE_PROJECT_NAME)_node

node-up: ## Lance le container
	docker-compose -f ${NODE_COMPOSE_FILE} up

node-build: ## Build el container
	docker-compose -f ${NODE_COMPOSE_FILE} build

node-stop: ## Arrête et supprime l’ensemble des éléments décrits dans le fichier de configuration de l'environnement mariadb (hors volumes)
	@docker-compose -f ${NODE_COMPOSE_FILE} stop

node-connect: ## Commande composer.
	$(CONNECT_NODE) bash
