#!/usr/bin/env bash

BASEDIR=$(dirname "$0")
echo "$BASEDIR"

cd "$BASEDIR/../../web/core"
ls -al

../../vendor/bin/phpunit ../modules/custom
