const BaseClass = require('../../tools/app/AppGenerator')
const fsCore = require("fs");
const path = require("path");
const slug = require('../../tools/io/io').slug
const glob = require("glob");

module.exports = class extends BaseClass {

	/**
	 * Dependencies.
	 * @return {string[]}
	 * @private
	 */
	_getDependencies() {
		return [
			'env'
		]
	}

	async main() {
		console.log("============ PHPUNIT");

		// Prompt if needed.
		this.options = await this.askQuestions(this.options)


		// makefile
		await this._linkPartials()

		// Tempaltes
		await this._copyTemplates()


	}

	/**
	 * Questinos.
	 * @return {{filter: (function(*=): *), name: string, type: string, message: string, required: boolean}[]}
	 * @private
	 */
	_getQuestions() {
		return [
			{
				type: "input",
				name: "projectId",
				message: "ID du projet (nom du répertoire, slug)",
				required: true,
				filter: (v) => {
					return slug(v)
				}
			},
			{
				type: "input",
				name: "rootDir",
				message: "Nom du répertoire applicatif",
				default: (options) => {
					return 'app'
				},
			},
		]
	}

	/**
	 * Ajout de la ligne dans le makefile.
	 * @return {Promise<void>}
	 * @private
	 */
	async _linkPartials() {
		// Copy des partials dans un répertoire de destination tmp
		const tmp = "tmp"+ Math.floor(Math.random()*1000)+"/";

		[
			{from: '*', to: null},
			{from: '.*', to: null},
			{from: '**/*', to: null},
			{from: '**/.*', to: null},
			{from: '.docker/**/*', to: './.docker'},
		].forEach((pattern, key) => {
			try {
				const dest = pattern.to ? this.destination(tmp + pattern.to) : this.destination(tmp)
				this.fs.copyTpl(
					this.sourceRoot()+'/../partials/'+pattern.from,
					dest,
					this.options,
				)
			} catch (e) {
				// console.log(key);
			}
		})


		await this.commitWriteFiles();
		// Ajout des éléments
		[
			'.*/**/*', '.*', '*', '**/*', '**/.*', '.*/**/.*'
		].forEach((dest) => {
			glob.sync(this.destination(tmp) + dest).forEach(file => {
				if( fsCore.lstatSync(file).isFile() ){
					const include = fsCore.readFileSync(file, 'utf8');
					const destFilePath = file.replace(tmp, '')
					if( fsCore.existsSync(destFilePath)){
						const content =  fsCore.readFileSync(destFilePath, 'utf8')

						if( content.indexOf( include ) < 0 ){
							fsCore.writeFileSync(destFilePath, content + include);
						}
					}
				}
			})
		})

		// Suppresion de tmp
		fsCore.rmSync(this.destination(tmp), { recursive: true, force: true });
	}
};
