const path = require('path')
const BaseClass = require('../../tools/app/AppGenerator')
const {slug} = require('../../tools/io/io')

module.exports = class extends BaseClass {

	/**
	 * Liste des paramètres.
	 * @return {{default: (String|*), name: string, type: string, message: string}[]}
	 * @private
	 */
	_getDependencies() {
		return ['prepare']
	}

	/**
	 * Liste des paramètres.
	 * @return {{default: (String|*), name: string, type: string, message: string}[]}
	 * @private
	 */
	_getQuestions() {
		const ids = this._getUserId();
		return [
			{
				type: "input",
				name: "projectId",
				message: "ID du projet (nom du répertoire, slug)",
				required: true,
				default: (options) => {
					return slug(options.projectName)
				},
				filter: (v) => {
					return slug(v)
				}
			},
			{
				type: "input",
				name: "userId",
				message: "User id (généralement 1000 sous linux)",
				default: ids.uid,
			},
			{
				type: "input",
				name: "groupId",
				message: "Group id (généralement 1000 sous linux)",
				default: ids.gid,
			},
			{
				type: "input",
				name: "nodeVersion",
				message: "Version de node",
				default: 15,
			},
			{
				type: "input",
				name: "secretLdapUser",
				message: "User LDAP",
			},
			{
				type: "password",
				name: "secretLdapPassword",
				message: "Mot de passe LDAP",
			},
		]
	}

	async main() {
		console.log("============ Environnement ");

		// Récupération des arguments
		this.options = await this.askQuestions(this.options);

		// Crétion du répertoire et copie des templates
		await this._copyTemplate();
	}

	/**
	 * Copy des templates
	 */
	async _copyTemplate() {

		// All dote files.
		this.fs.copyTpl(
			this.destination('./.env.example'),
			this.destination('./.env'),
			this.options,
		)

		return this.commitWriteFiles();
	}

	/**
	 * Récupère les valeurs d'utilisateurs courant.
	 * @return {{}}
	 * @private
	 */
	_getUserId() {
		var child = require('child_process').execSync('id')
		const values = {}
		const data = child.toString().split(' ')
			.map(item => {
				return item.split('(')[0].split('=')
			}).forEach(item => {
				values[item[0]] = item[1]
			})
		return values
	}
};
