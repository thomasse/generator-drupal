const BaseClass = require('../../tools/app/AppGenerator')

module.exports = class extends BaseClass {

    _getDependencies(){
        return [
            'module',
            'theme',
        ]
    }

    _getQuestions() {
        return [
            {
                type: "input",
                name: "projectId",
                message: "Id du projet",
            },
            {
                type: "input",
                name: "projectName",
                message: "Nom du projet",
            },
            {
                type: "input",
                name: "rootDir",
                message: "Nom du répertoire applicatif",
                default: (options) => {
                    return 'app'
                },
            },
            {
                type: "input",
                name: "profileName",
                message: "Nom du profile",
                default: options => {
                    return (options.projectId || this.options.projectId) + '_profile';
                }
            },
            {
                type: "input",
                name: "themeName",
                message: "Nom du theme",
                default: options => {
                    return (options.projectId || this.options.projectId) + '_theme';
                }
            },
            {
                type: "input",
                name: "moduleName",
                message: "Nom du module",
                default: options => {
                    return (options.projectId || this.options.projectId) + '_module';
                }
            },
        ]
    }

    async main() {
        console.log("============ Profil d'installation ");

        // Récupération des arguments
        this.options = await this.askQuestions(this.options);

        // Crétion du répertoire et copie des templates
        await this._copyTemplate();
    }

    /**
     * Copy des templates
     */
    async _copyTemplate() {

        // All dote files.
        try {
            this.fs.copyTpl(
                this.templatePath('./**/*'),
                this.destination(),
                this.options,
            )
        }
        catch(e){}

        try{
            this.fs.copyTpl(
                this.templatePath('./**/.*'),
                this.destination(),
                this.options,
            )
        }
        catch(e){}

        return this.commitWriteFiles();
    }
};
