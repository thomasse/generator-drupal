const path = require('path')
const fsCore = require('fs')
const BaseClass = require('../../tools/app/AppGenerator')
const {slug} = require('../../tools/io/io')

module.exports = class extends BaseClass {

	async main() {
		console.log("============ Prepare ")

		// Récupération des arguments
		this.options = await this.askQuestions(this.options)

		// Initialisation du repertoire de travail.
		this.initDestination()

		// Crétion du répertoire et copie des templates
		await this._copyTemplate()
	}

	/**
	 * Liste des paramètres.
	 * @return {{default: (String|*), name: string, type: string, message: string}[]}
	 * @private
	 */
	_getQuestions() {
		const ids = this._getUserId();
		return [
			{
				type: "input",
				name: "projectName",
				message: "Nom du projet",
				required: true,
			},
			{
				type: "input",
				name: "projectId",
				message: "ID du projet (nom du répertoire, slug)",
				required: true,
				default: (options) => {
					return slug(options.projectName)
				},
				filter: (v) => {
					return slug(v)
				}
			},
			{
				type: "input",
				name: "projectDescription",
				message: "Description du projet",
				default: (options) => {
					return 'Projet Drupal : ' + options.projectName
				},
			},
			{
				type: "input",
				name: "rootDir",
				message: "Nom du répertoire applicatif",
				default: (options) => {
					return 'app'
				},
			},
			{
				type: "input",
				name: "userId",
				message: "User id (généralement 1000 sous linux)",
				default: ids.uid,
			},
			{
				type: "input",
				name: "groupId",
				message: "Group id (généralement 1000 sous linux)",
				default: ids.gid,
			},
			{
				type: "input",
				name: "nodeVersion",
				message: "Version de node",
				default: 15,
			},
		]
	}

	/**
	 * Copy des templates
	 */
	async _copyTemplate() {

		// On ajoute un tricks sur les options pour ne pas générer les secrets.
		this.options.secretLdapUser = '<%= secretLdapUser %>'
		this.options.secretLdapPassword = '<%= secretLdapPassword %>'

		// All files.
		this.fs.copyTpl(
			this.templatePath('./**/*'),
			this.destination(),
			this.options,
		)

		// All dote files.
		this.fs.copyTpl(
			this.templatePath('./**/.*'),
			this.destination(),
			this.options,
		)


		return this.commitWriteFiles();
	}

	/**
	 * Initialise la destination.
	 */
	initDestination() {
		try {
			this.destination()
		} catch (e) {
			this.setWorkDir(path.join(this.destinationPath(), this.options.projectId));
		}
	}

	/**
	 * Récupère les valeurs d'utilisateurs courant.
	 * @return {{}}
	 * @private
	 */
	_getUserId() {
		var child = require('child_process').execSync('id')
		const values = {}
		const data = child.toString().split(' ')
			.map(item => {
				return item.split('(')[0].split('=')
			}).forEach(item => {
				values[item[0]] = item[1]
			})
		return values
	}

	/**
	 *  Make a symbolic link within `#destinationRoot()`.
	 *
	 *  @method     _symLink
	 *
	 *  @param      {String}        src         The path relative to `#destinationRoot()` that should become the target of the symbolic link.
	 *  @param      {String}        dst         The path relative to `#destinationRoot()` that should become the symbolic link.
	 *
	 */
	_symLink(src, dst) {
		try {
			fsCore.unlinkSync(dst);
		} catch (error) {
			if ('ENOENT' !== error.code) throw error;
		}
		fsCore.symlinkSync(path.relative(path.dirname(dst), src), dst);
	}
};
