
## PHPStorm
#### Ajouter des watchers sur les fichiers yml et lancer un drush cr.
Pour automatiser le drush cr en fonction d'une modif de fichiers yml de module.

1. Aller dans les paramètres > Tools > File Watcher
2. Ajouter un watcher "<custom>"
3. Créer un scop dédié aux modules :
   1. Créer un scope "Local"
   2. Nommer "Modules"
   3. Renseigner la pattern "file[rcibs]:www/web/modules/custom/*/*.yml"

4. Renseigner
   - Nom: Services (ou ce que vous voulez)
   - Type: Yml
   - Scope: Modules
   - Program: /bin/make
   - Arguments: drush cr
   - Working directory: $ProjectFileDir$
   - Show console: on error

## Xdebug
la source d'un article qui décrit comment configurer xdebug 3 et phpstorm
https://matthewsetter.com/setup-step-debugging-php-xdebug3-docker/
### Installation
Pour utiliser Xdebug il aut au préalable:
1. Installer xdebug
   - `make xdebug-install`
2. Remplacer la config de base par la config voulu qui est présente dans le fichier .docker/drupal/apache/xdebug/xdebug.ini
   - `make xdebug-replace-ini`
3. Une fois installer, il y a possibilité de désactiver/activer xdebug
   - `make xdebug-disable`
   - `make xdebug-enable`

Pour s'assurer que xdebug est bien installé sur le container
1. Se connecter au container
   - `make server-connect`
2. Lancer la commande php
   - `php -v`

Le résultat doit être faire apparaitre la version de Xdebug

![Php -v with Xdebug](./img/bash_php_version_with_xdebug.png)

### Xdebug avec Phpstorm

Prérequis:
- Xdebug version 3
- Phpstom version 2020.3 ou plus

Une fois l'installation de Xdebug:
- Ouvrir le projet dans phpstorm
- Dans Settings > PHP > Debug

![Setup phpstorm](./img/xdebug_phpstorm_setup.png)
- Cocher la case `Break at first line in PHP scripts` pour savoir si le debug par étapes fonctionne au premier essai, quand aucun point d'arrêt n'est défini

- Vérification que le debug fonctionne en démarrant l'écoute

![active_debug_phpstorm](./img/phpstorm_active_debug.png)

![active_debug_phpstorm](./img/phpstorm_active_debug_listen.png)

- Accédez à l'url du projet dans le navigateur
- Vous devriez voir que la requête ne se termine pas dans votre navigateur
- Et que Phpstorm affiche l'index.php du projet (Pour Drupal)

![active_debug_phpstorm](./img/phpstorm_step_by_step_actived.png)

- Xdebug est fonctionnelle et la case `Break at first line in PHP scripts` peut être désactivé

- Le debug avec les points d'arrêt peut être mis en place pour debugger le code voulu

![active_debug_phpstorm](./img/phpstorm_point.png)

- La console de debug affiche toutes les infos et la trace de debug

![active_debug_phpstorm](./img/phpstorm_debug_console.png)


## Tips debug

- Le bouton Listen permet de débuter l'écoute et passe du rouge au vert lors de l'écoute

- Le bouton stop permet de stopper l'écoute en cours
- Un réactualisation de l'url a écouter redémarre le step by step

- Basculer le bouton Listen de vert à rouge pour arrêter complétement l'écoute

- Attention lorsque le bouton Listen est activé, les commandes passé dans le terminal du container seront aussi écoutées
