# Comment installer le projet ?

## Vous possédez yeoman et le générateur drupal :
1. Utilisez la commande `make dev-install`.
   Le script vous demandera vos identifiants LDAP pour générer le .env. Dans l'idéal, utilisez les identifiants ldap 
   liés au projet (voir variables gitlab).
   
### Si vous voulez installer yeoman :
Reportez vous au repo [https://bitbucket.org/thomasse/generator-drupal/](https://bitbucket.org/thomasse/generator-drupal)


## Vous ne possédez pas yeoman et le générateur drupal :
1. Copiez et renommez le fichier .env.example en .env
2. modifiez les variables nécessaires ('<'% $$$$ %'>') par les valeurs nécessaires.
3. Lancez la commande  `make dev-install`.
