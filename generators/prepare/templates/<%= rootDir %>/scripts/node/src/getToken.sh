#!/bin/bash

registry=${1}
file=${2}
user=${3}
password=${4}

result=$(curl -s \
  -H "Accept: application/json" \
  -H "Content-Type:application/json" \
  -X PUT --data "{\"name\": \"$user\", \"password\": \"$password\"}" \
  $registry-/user/org.couchdb.user:$user)
echo $result
