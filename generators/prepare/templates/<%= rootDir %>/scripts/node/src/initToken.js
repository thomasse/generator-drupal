const {exec} = require("child_process");

// Récupération des arguments de la commande.
const args = process.argv.slice(2);
const scope = '@consertotech';
const registry = '//repo.consertotech.pro/repository/npm-dev/';
const registryUrl = 'https://repo.consertotech.pro/repository/npm-dev/';
const user = args[0] || process.env.NODE_CONSERTO_USER;
const password = args[1] || process.env.NODE_CONSERTO_PASSWORD;
const file = `${__dirname}/token.json`;


/**
 * Execute une commande.
 * @param cmd
 * @return {Promise<unknown>}
 */
function doExec(cmd) {
    const callback = (resolve, reject) => {
        exec(cmd, (error, stdout, stderr) => {
            if (error) {
                reject(error);
            }
            if (stderr) {
                reject(stderr);
            }
            resolve(stdout);
        })
    }

    return new Promise(callback);
}

/**
 * Initialie le token nexus.
 * @param token
 * @param cb
 */
function initNPMToken(token, cb) {
    console.log("=> Initialisation du registre.", token)
    if (token) {
        doExec(`npm config set ${scope}:registry "${registryUrl}"  --scope=${scope}`)
            .then((value) => {
                console.log(`Récupération OK : ${token}`);
                doExec(`npm set ${registry}:_authToken ${token}`)
                    .then(() => {
                        console.log(`Initialisation OK : ${token}`);
                        cb();
                    })
                    .catch(err => {
                        console.log(`Initialisation KO`);
                        console.error(error)
                        cb();
                    })
            })
            .catch(
                error => {
                    console.log(`Récupération KO`);
                    console.error(error)
                    cb();
                }
            )
    } else {
        console.error("Pas de token");
    }
}

/**
 * Récupère le token auprès du nexus.
 * @param cb
 */
function getToken(cb) {
    console.log("=> Récupération du token depuis NEXUS.")
    const cmd = `${__dirname}/getToken.sh ${registryUrl} ${file} ${user} ${password}`;
    doExec(cmd)
        .then(
            (result) => {
                initNPMToken(JSON.parse(result).token, cb)
            }
        )
        .catch(
            error => console.error(error)
        )
}


exports.getToken = getToken;
exports.doExec = doExec;

