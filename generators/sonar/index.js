const BaseClass = require('../../tools/app/AppGenerator')
const fsCore = require("fs");
const path = require("path");
const slug = require('../../tools/io/io').slug

module.exports = class extends BaseClass {

	/**
	 * Dependencies.
	 * @return {string[]}
	 * @private
	 */
	_getDependencies() {
		return [
			'env'
		]
	}

	async main() {
		console.log("============ SonarQube");

		// Prompt if needed.
		this.options = await this.askQuestions(this.options)

		// Copy templates.
		await this._copyTemplates()

		// ci
		await this._addCI()
	}

	/**
	 * Questinos.
	 * @return {{filter: (function(*=): *), name: string, type: string, message: string, required: boolean}[]}
	 * @private
	 */
	_getQuestions() {
		return [
			{
				type: "input",
				name: "projectId",
				message: "ID du projet (nom du répertoire, slug)",
				required: true,
				filter: (v) => {
					return slug(v)
				}
			},
			{
				type: "input",
				name: "rootDir",
				message: "Nom du répertoire applicatif",
				default: (options) => {
					return 'app'
				},
			},
		]
	}

	async _addCI() {
		// Récupération du contenu existant
		const envFilePath = this.destination('.gitlab-ci.yml')
		const include = fsCore.readFileSync(this.destination(`.gitlab-ci.example.yml`), 'utf8')
		const content = fsCore.readFileSync(envFilePath, 'utf8')
		fsCore.unlinkSync(this.destination(`.gitlab-ci.example.yml`))

		if (content.indexOf(include) < 0) {
			fsCore.writeFileSync(envFilePath, content + include);
		}
	}
};
