#!/bin/bash

# Récupération du token.
yarn install
yarn run token ${1} ${2}

# Placement dans le répertoire de travail.
cd ${3}
npm i
./gulp
