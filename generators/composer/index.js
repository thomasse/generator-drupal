const fsCore = require('fs')
const BaseClass = require('../../tools/app/AppGenerator')

module.exports = class extends BaseClass {

    async main(){
        // Prompt if needed.
        this.options = await this.askQuestions(this.options);

        console.log("============ Composer");

        // Tempaltes
        await this._copyCreateProjectScript()

        // Create project
        this._createProject()

        // Suppression de l'existant
        this._deleteBuildRessources()

        // Ajout des dépendances
        this._addDependencies()

        // Copy des fichiers de l'app.
        await this._copyAppFiles();

        // Création des symlinks
        await this._createSymbolicLinks()
    }

    _getDependencies(){
        return [
            'docker'
        ]
    }

    _getQuestions() {
        return [
            {
                type: "input",
                name: "projectName",
                message: "Nom du projet",
                default: this.appname // Default to current folder name
            },
            {
                type: "input",
                name: "projectId",
                message: "Id du projet",
                default: this.appname // Default to current folder name
            },
            {
                type: "input",
                name: "rootDir",
                message: "Nom du répertoire applicatif",
                default: (options) => {
                    return 'app'
                },
            },
        ]
    }

    /**
     * Copie des fichiers de conf.
     *
     * @return {Promise<unknown>}
     * @private
     */
    async _copyCreateProjectScript() {
        this.fs.copyTpl(
            this.templatePath('./create_project.sh'),
            this.destination(`./${this.options.rootDir}/create_project.sh`),
            this.options,
        )

        return this.commitWriteFiles();
    }

    /**
     * Create les sources du project.
     */
     _createProject() {
         const args = `exec -i --user www-data ${this.options.projectId}_server bash ./create_project.sh`
         this.spawnCommandSync('docker', args.split(' '), {cwd:this.destination()})
    }

    /**
     * Supprimes le scaffold inutile.
     */
    _deleteBuildRessources() {
        const composerJSON = require(this.destination(`./${this.options.rootDir}/composer.json`));
        const modulesToRemove = Object.keys(composerJSON.require)
        if( modulesToRemove.length ){
            this.spawnCommandSync('make', ['composer', 'remove', ...modulesToRemove], {cwd:this.destination()})
        }
    }

    /**
     * Ajout des dépendances.
     * @private
     */
    _addDependencies() {
        const composerModules = require(this.templatePath('./modules/modules.json'))

        // Config composer.
        this.spawnCommandSync('make' , ['composer', 'config', 'minimum-stability', 'dev'], {cwd:this.destination()});
        this.spawnCommandSync('make' , ['composer', 'config', 'extra', 'drupal-core-project-message', ''], {cwd:this.destination()});
        let args = `exec -i --user www-data ${this.options.projectId}_server php -d memory_limit=-1 /usr/local/bin/composer config repositories.lycanthrop composer https://repo.consertotech.pro/repository/composerto-drupal`
        this.spawnCommandSync('docker', args.split(' '), {cwd:this.destination()})

        // Require
        this.spawnCommandSync('make', ['composer', 'require', composerModules.require.join(' ')], {cwd:this.destination()})

        // Require dev.
        args = `exec -i --user www-data ${this.options.projectId}_server php -d memory_limit=-1 /usr/local/bin/composer require --dev ${composerModules['require-dev'].join(' ')}`
        this.spawnCommandSync('docker', args.split(' '), {cwd:this.destination()})
    }

    /**
     * crée les liens symboliques.
     * @private
     */
    _createSymbolicLinks() {

        console.log("Create symlinks");
        this._symLink(this.destination('data/.env'), this.destination(`${this.options.rootDir}/.env`), true);
        this._symLink(this.destination('data/web/.htaccess'), this.destination(`${this.options.rootDir}/web/.htaccess`), true);

        this._symLink(this.destination('data/web/sites/default/files'), this.destination(`${this.options.rootDir}/web/sites/default/files`))
        this._symLink(this.destination('data/privates'), this.destination(`${this.options.rootDir}/privates`))
        this._symLink(this.destination('data/web/sites/default/settings.local.php'), this.destination(`${this.options.rootDir}/web/sites/default/settings.local.php`))

        return this.commitWriteFiles();
    }

    /**
     * Copie des fichiers de conf.
     *
     * @return {Promise<unknown>}
     * @private
     */
    async _copyAppFiles() {

        // Force override en supprimant les fichiers générés par drupal.
        [
            this.destination(`${this.options.rootDir}/web/sites/example.settings.local.php`),
            this.destination(`${this.options.rootDir}/web/sites/default/default.services.yml`),
            this.destination(`${this.options.rootDir}/web/sites/default/default.settings.php`),
        ].map(file => {
            fsCore.unlinkSync(file)
        })


        this.fs.copyTpl(
            this.templatePath('./app/**'),
            this.destination(`./${this.options.rootDir}`),
            this.options,
        )

        return this.commitWriteFiles();
    }

};
