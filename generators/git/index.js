const BaseClass = require('../../tools/app/AppGenerator')
const {execSync} = require("child_process");

module.exports = class extends BaseClass {

	/**
	 * Liste des paramètres.
	 * @return {{default: (String|*), name: string, type: string, message: string}[]}
	 * @private
	 */
	_getDependencies() {
		return ['install']
	}

	/**
	 * Liste des paramètres.
	 * @return {{default: (String|*), name: string, type: string, message: string}[]}
	 * @private
	 */
	_getQuestions() {
		return [
		]
	}

	async main() {
		console.log("============ Initialisation git");

		// Récupération des arguments
		this.options = await this.askQuestions(this.options);

		// Crétion du répertoire et copie des templates
		this._gitInit();
	}

	/**
	 * Git init.
	 */
	_gitInit() {
		// All dote files.
		execSync('git init', {cwd: this.destination()})
		execSync('git checkout -b develop', {cwd: this.destination()})
		execSync('git add .', {cwd: this.destination()})
		execSync('git commit -m "Initial commit"', {cwd: this.destination()})
	}

};
