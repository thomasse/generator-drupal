const path = require('path')
const BaseClass = require('../../tools/app/AppGenerator')
const {slug} = require('../../tools/io/io')

module.exports = class extends BaseClass {

	/**
	 * Liste des paramètres.
	 * @return {{default: (String|*), name: string, type: string, message: string}[]}
	 * @private
	 */
	_getDependencies() {
		return ['profile']
	}

	/**
	 * Liste des paramètres.
	 * @return {{default: (String|*), name: string, type: string, message: string}[]}
	 * @private
	 */
	_getQuestions() {
		return [
			{
				type: "input",
				name: "projectId",
				message: "Id du projet",
			},
			{
				type: "input",
				name: "rootDir",
				message: "Nom du répertoire applicatif",
				default: (options) => {
					return 'app'
				},
			},
			{
				type: "input",
				name: "profileName",
				message: "Nom du profile",
				default: options => {
					return (options.projectId || this.options.projectId) + '_profile';
				}
			},
		]
	}

	async main() {
		console.log("============ Installation du site ");

		// Récupération des arguments
		this.options = await this.askQuestions(this.options);

		// Crétion du répertoire et copie des templates
		await this._createHash();

		// Crétion du répertoire et copie des templates
		await this._installProfile();

		// Export la db d'install
		await this._initDB();

		// Export la configuration.
		await this._initConf();
	}

	/**
	 * Copy des templates
	 */
	async _installProfile() {

		// All dote files.
		return this.spawnCommandSync('make', ['site-install', this.options.profileName], {cwd: this.destination()});
	}

	/**
	 * Création du hash.
	 * @return {Promise<unknown>}
	 * @private
	 */
	async _createHash() {
		// Création du hash.
		const shasum = require('crypto').createHash('sha1')
		shasum.update(this.options.projectId)
		const hashsalt = shasum.digest('hex')


		// All dote files.
		try {
			this.fs.copyTpl(
				this.templatePath('./**/*'),
				this.destination(),
				{...this.options, ...{hashsalt: hashsalt}},
			)
		} catch (e) {
		}

		return this.commitWriteFiles();
	}

	/**
	 * Export la db d'install.
	 * @return {Promise<*>}
	 * @private
	 */
	async _initDB() {
		return this.spawnCommandSync('make', ['db-init-export'], {cwd: this.destination()});
	}

	/**
	 * Export de la conf
	 * @return {Promise<*>}
	 * @private
	 */
	async _initConf() {
		return this.spawnCommandSync('make', ['drush', 'cex'], {cwd: this.destination()});
	}

};
