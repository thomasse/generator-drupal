const BaseClass = require('../../tools/app/AppGenerator')
const {execSync} = require("child_process");

module.exports = class extends BaseClass {

    /**
     * Liste des paramètres.
     * @return {{default: (String|*), name: string, type: string, message: string}[]}
     * @private
     */
    _getDependencies() {
        return ['profile']
    }

    /**
     * Liste des paramètres.
     * @return {{default: (String|*), name: string, type: string, message: string}[]}
     * @private
     */
    _getQuestions() {
        return [
            {
                type: 'confirm',
                name: 'is_sandbox',
                message: "C'est une sandbox ?",
                initial: false
            },
            {
                type: "input",
                name: "rootDir",
                message: "Nom du répertoire applicatif",
                default: (options) => {
                    return 'app'
                },
            },
        ]
    }

    async main() {
        console.log("============ Initialisation git");

        // Récupération des arguments
        this.options = await this.askQuestions(this.options);

        // Crétion du répertoire et copie des templates
        if (this.options.is_sandbox) {
            const modules = require(this.templatePath('./modules.json'))
            Object.keys(modules).forEach(type => {
                Object.keys(modules[type]).forEach(rep => {
                    const path = this.options.rootDir + '/web/' + type + '/' + rep;
                    this.spawnCommandSync('mkdir', [path, '-p'], {cwd: this.destination()});
                    modules[type][rep].forEach(item => {
                        // composer uninstal
                        this.spawnCommandSync('make', ['composer', 'remove', item.composer], {cwd: this.destination()})
                        // git clone.
                        this.spawnCommandSync('git', ['clone', item.repo], {cwd: this.destination()+'/'+path})
                    })
                })
            })
        }
    }
};
