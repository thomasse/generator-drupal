// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
module.exports = (on, config) => {

  // Initialisation de l'url de base en fonction de l'environnement.
  const options = {
    env: {
      siteUrlPropertyName: 'SITE_DOMAIN',
    }
  }
  const envUtils = new (require('./utils/env-utils'))(on, config, options)
  // Get the domain to test.
  const envBaseUrl = envUtils.getBaseUrl()
  let envBaseUrlProvenance = 'cypress.json'
  if (envBaseUrl) {
    config.baseUrl = envBaseUrl.url
    envBaseUrlProvenance = envBaseUrl.provenance
  }

  console.log("===================================")
  console.log("     Base url : " + config.baseUrl)
  console.log("     Provenance : " + envBaseUrlProvenance )
  console.log("===================================")

  return config
}

