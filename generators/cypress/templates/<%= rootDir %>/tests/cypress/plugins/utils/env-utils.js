const path = require("path");

module.exports = class {
  constructor(on, config, options =  {}) {
    this.config = config;
    this.options = {
      ...this.getDefaultOptions(),
      ...options,
    }
  }

  /**
   * Return the default options.
   *
   * @return {{env: {siteUrlPropertyName: string}}}
   */
  getDefaultOptions(){
    return {
      env: {
        siteUrlPropertyName: 'CYPRESS_BASE_URL',
      }
    }
  }

  /**
   * Return the base url.
   * @return {*|boolean}
   */
  getBaseUrl(){
    let baseUrl =  this.getCommandLineBaseUrl() || this.getDotEnvFileBaseUrl()
    if(baseUrl){
      if(baseUrl.url.indexOf('http') !== 0){
        baseUrl.url = 'https://' + baseUrl.url
      }
    }
    return baseUrl
  }


  /**
   * Return the local domain to test in the project .env
   * @return {boolean}
   */
  getDotEnvFileBaseUrl() {
    let localBaseUrl = false;
    try {
      const envPath = path.join(process.cwd(), '../../.env')
      const envData = require('dotenv').config({path: envPath}).parsed
      if( envData[this.options.env.siteUrlPropertyName] ){
        localBaseUrl = {
          url: envData[this.options.env.siteUrlPropertyName],
          provenance: '.env',
        }
      }

    } catch (e) {
      // No site domain in .env found.
    }
    return localBaseUrl;
  }

  /**
   * Return the base url from the command line.
   * @return {*|boolean}
   */
  getCommandLineBaseUrl(){
    let commandLineBaseUrl = false
    if( this.config?.env.baseUrl ){
      commandLineBaseUrl = {
        url: this.config?.env.baseUrl,
        provenance: "commandLine",
      }
    }
    return commandLineBaseUrl
  }
}
