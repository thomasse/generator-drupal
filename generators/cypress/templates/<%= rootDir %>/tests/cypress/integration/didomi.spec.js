describe('didomi', () => {

  it('show_didomi', () => {
    cy.visit('/')
    cy.get('#didomi-popup').should('be.visible')
  })

  it('refuse_didomi', () => {
    cy.get('#didomi-notice-disagree-button').click()
    cy.get('#didomi-popup').should('not.exist')
  })

})
