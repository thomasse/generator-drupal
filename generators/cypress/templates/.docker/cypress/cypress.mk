##
#############################    node    #####################################7

CYPRESS_DIR = '.'

DOCKER_COMPOSE_CYPRESS = @docker-compose -f ${CYPRESS_COMPOSE_FILE} ${DOCKER_REALTIVE_ENV_FILE}

cypress-run: ## Lance cypress en local sur domaine local (lié au .env), ex: make cypress-run
	cd ${CYPRESS_TEST_DIR} && npm i && ./node_modules/cypress/bin/cypress run

cypress-run-on: ## Lance cypress en local sur un domain passé, ex: make cypress-run-on https://www.test.com
	cd ${CYPRESS_TEST_DIR} && npm i && ./node_modules/cypress/bin/cypress run --env baseUrl=$(filter-out $@,$(MAKECMDGOALS))

cypress-open: ## Ouvre la fenêtre de test en local, ex: make cypress-open
	cd ${CYPRESS_TEST_DIR} && npm i && ./node_modules/cypress/bin/cypress open

cypress-open-on: ## Ouvre la fenêtre de test en local sur un domain passé, ex: make cypress-open-on https://www.test.com
	cd ${CYPRESS_TEST_DIR} && npm i && ./node_modules/cypress/bin/cypress open --env baseUrl=$(filter-out $@,$(MAKECMDGOALS))

cypress-build: ## Lance cypress via le container pour test ci.
	docker build -t $(COMPOSE_PROJECT_NAME)_cypress -f .docker/cypress/cypress/Dockerfile . --build-arg BASE_URL=${CYPRESS_BASE_URL} --no-cache

cypress-build-on: ## Lance cypress via le container pour test ci.
	docker build -t $(COMPOSE_PROJECT_NAME)_cypress -f .docker/cypress/cypress/Dockerfile . --build-arg BASE_URL=$(filter-out $@,$(MAKECMDGOALS)) --no-cache
