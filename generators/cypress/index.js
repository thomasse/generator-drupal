const BaseClass = require('../../tools/app/AppGenerator')
const fsCore = require("fs");
const path = require("path");
const slug = require('../../tools/io/io').slug

module.exports = class extends BaseClass {

	/**
	 * Dependencies.
	 * @return {string[]}
	 * @private
	 */
	_getDependencies() {
		return [
			'env'
		]
	}

	async main() {
		console.log("============ Cypress");

		// Prompt if needed.
		this.options = await this.askQuestions(this.options)

		// Tempaltes
		await this._copyTemplates()

		// Symlink
		await this._creatSymlink()

		// makefile
		await this._linkMakefile()

		// env
		await this._addEnv()

		// ci
		await this._addCI()
	}

	/**
	 * Questinos.
	 * @return {{filter: (function(*=): *), name: string, type: string, message: string, required: boolean}[]}
	 * @private
	 */
	_getQuestions() {
		return [
			{
				type: "input",
				name: "projectId",
				message: "ID du projet (nom du répertoire, slug)",
				required: true,
				filter: (v) => {
					return slug(v)
				}
			},
			{
				type: "input",
				name: "rootDir",
				message: "Nom du répertoire applicatif",
				default: (options) => {
					return 'app'
				},
			},
		]
	}

	/**
	 * Symlink pour le raccourci vers cypress.
	 * @return {Promise<void>}
	 * @private
	 */
	async _creatSymlink() {
		// front theme
		let dest = this.destination(`${this.options.rootDir}/tests/node_modules/cypress/bin/cypress`)
		fsCore.mkdirSync(path.dirname(dest), {recursive: true});
		fsCore.writeFileSync(dest, '')
		this._symLink(
			dest,
			this.destination(`${this.options.rootDir}/tests/test`)
		);
		fsCore.unlinkSync(dest)
	}

	/**
	 * Ajout de la ligne dans le makefile.
	 * @return {Promise<void>}
	 * @private
	 */
	async _linkMakefile() {
		// Récupération du contenu existant
		const makefilePath = this.destination('Makefile')
		const include = 'include .docker/cypress/cypress.mk\r\n';
		const content =  fsCore.readFileSync(makefilePath, 'utf8')

		if( content.indexOf( include ) < 0 ){
			fsCore.writeFileSync(makefilePath, include + content);
		}
	}

	async _addEnv(){
		['.env', '.env.example'].forEach(filePath => {
			// Récupération du contenu existant
			const envFilePath = this.destination(filePath)
			const include = `CYPRESS_TEST_DIR=${this.options.rootDir}/tests\r\n`
			const content =  fsCore.readFileSync(envFilePath, 'utf8')

			if( content.indexOf( include ) < 0 ){
				fsCore.writeFileSync(envFilePath,  content + include);
			}
		})
	}

	async _addCI(){
		// Récupération du contenu existant
		const envFilePath = this.destination('.gitlab-ci.yml')
		const include = fsCore.readFileSync(this.destination(`.docker/cypress/.gitlab-ci.example.yml`), 'utf8')
		const content =  fsCore.readFileSync(envFilePath, 'utf8')

		if( content.indexOf( include ) < 0 ){
			fsCore.writeFileSync(envFilePath,  content + include);
		}
	}
};
