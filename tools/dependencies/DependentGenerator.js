const QuestionGenerator = require('../io/QuestionGenerator')

module.exports = class extends QuestionGenerator{

    /**
     * Abstract méthod.
     *
     * @return {*[]}
     * @private
     */
    _getDependencies(){
        return []
    }

    /**
     * Retourne les dépendances
     *
     * @return {*[]}
     */
    getDependencies(){
        return this._getDependencies();
    }

}
