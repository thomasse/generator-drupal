module.exports.sortByDependencies = function (elements) {

    // Récupération des instances
    elements = elements.map(item => {
        const generatorClass = require(item.path);
        item.instance = new generatorClass({}, {});
        if( typeof item.instance.getDependencies === 'function'){
            item.dependencies = item.instance.getDependencies()
        }
        return item;
    }).filter(item => {
        return item.dependencies
    })

    // sort par dépendance.
    let sorted = [];
    for (const element of elements) {
        addElementInSortedDependencies(element, sorted, elements)
    }

    // unique
    sorted = unique(sorted)

    // Erreur de dépendances.
    if (sorted.length !== elements.length) {
        throw "Des dépendances n'ont pas pu être traitées"
    }

    // Ajout des elements.
    return sorted.map(name => {
        return elements.filter(element => element.name === name)[0]
    })
}

/**
 * Ajoute l'élément dans sorted.
 * @param element
 * @param sorted
 * @param allElements
 * @return {number}
 */
function addElementInSortedDependencies(element, sorted, allElements) {
    let maxIndex = -1

    if (element.dependencies.length > 0) {
        const indexes = []
        for (const dependency of element.dependencies) {
            const index = sorted.indexOf(dependency)
            // Si l'élément est déjà présent dans sorted.
            if (index > -1) {
                indexes.push(index)
            } else {
                // Sinon on récupère l'élément de la dépendance et on l'insère.
                const dependencyElement = allElements.filter(item => item.name === dependency)[0];
                indexes.push(addElementInSortedDependencies(dependencyElement, sorted, allElements))
            }
        }

        maxIndex = Math.max(...indexes)
    }

    sorted.splice(maxIndex + 1, 0, element.name);

    return maxIndex + 1;
}

/**
 * Unique the array.
 *
 * @param list
 * @return {*}
 */
function unique(list) {
    return list.filter((value, index, self) => {
        return self.indexOf(value) === index;
    })
}

/**
 * Retourne la liste de selection.
 *
 * @param selection
 * @param allDependencies
 */
function getAllDependencies(element, allElements) {
    const dependencies = element.dependencies
    for (const dep of element.dependencies) {
        const e = allElements.filter(item => item.name === dep)[0];
        if (e) {
            dependencies.push(getAllDependencies(e, allElements))
        }
    }
    return unique(dependencies.join(',').split(',')).filter(item => item.length > 0)
}

module.exports.getGeneratorsAndDependencies = function (selection, allDependencies) {
    const direct = selection.map(element => element.name)
    const hidden = selection.map(element => getAllDependencies(element, allDependencies)).join(',').split(',')

    return allDependencies
        .map(element => {
            if (direct.indexOf(element.name) > -1) {
                element.action = `${element.name}`
            } else if (hidden.indexOf(element.name) > -1) {
                element.action = `${element.name} (dependence)`
            }
            return element
        })
        .filter(element => {
            return typeof element.action == 'string'
        })
}
