const path = require('path')
const fs = require('fs')


module.exports = {

	/**
	 * Répertoire drupal
	 */
	_yoFileName: '.yo-drupal',

	/**
	 * App Dir.
	 */
	_appDir: false,

	/**
	 * REcherche dans l'arborescence du répertoire de travail.
	 * @param dir
	 */
	initAppDir(dir) {

		this._appDir = this.getWorkDir(dir)
		if (!this._appDir) {
			throw "Pas de projet trouvé. Lancez la commande dans un répertoire projet drupal";
		}
	},

	/**
	 * Retourne vrai si le répertoire est le répertoire de travail.
	 * @param dir
	 * @return {boolean}
	 */
	isWorkDir(dir) {
		const f = path.join(dir, this._yoFileName)
		return fs.existsSync(f)
	},

	/**
	 * Retourne le répertoire de travail selon l'arborescence.
	 * @param dir
	 */
	getWorkDir(dir) {
		let workDir = false;
		do {
			if (this.isWorkDir(dir)) {
				workDir = dir;
			} else {
				dir = path.dirname(dir);
			}
		}
		while (dir != '/' && !workDir)

		return workDir
	},

	/**
	 * Récupération du appDir.
	 * @return {boolean}
	 */
	get appDir() {
		return this._appDir
	},

	/**
	 * Force app dir.
	 * @param dir
	 */
	set appDir(dir) {
		this._appDir = dir
	}

}
