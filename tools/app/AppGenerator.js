const BaseClass = require('../dependencies/DependentGenerator')
const AppPath = require('./AppPath')
const path = require('path')
const fsCore = require('fs')

/**
 * App Path Service.
 */
module.exports = class extends BaseClass {

	/**
	 * Retourne la destination depuis le workdir.
	 * @param dest
	 * @return {string}
	 */
	destination(dest='') {
		if (!AppPath.appDir) {
			AppPath.initAppDir(this.destinationPath());
		}

		return path.join(AppPath.appDir, dest);
	}

	/**
	 * Force le répertoire de travail.
	 * @param dir
	 */
	setWorkDir(dir){
		AppPath.appDir = dir
	}


	/**
	 *  Make a symbolic link within `#destinationRoot()`.
	 *
	 *  @method     _symLink
	 *
	 *  @param      {String}        src         The path relative to `#destinationRoot()` that should become the target of the symbolic link.
	 *  @param      {String}        dst         The path relative to `#destinationRoot()` that should become the symbolic link.
	 *
	 */
	_symLink(src, dst, save = false) {
		try {
			if (save){
				fsCore.copyFileSync(dst, dst+'.example')
			}
			fsCore.unlinkSync(dst);
		} catch (error) {
			if ('ENOENT' !== error.code) throw error;
		}
		fsCore.symlinkSync(path.relative(path.dirname(dst), src), dst);
	}

	/**
	 * Copy des templates
	 */
	async _copyTemplates() {
		// Copy des templates
		[
			{from: './**/*', to: null},
			{from: './**/.*', to: null},
			{from: './.docker/**/*', to: './.docker'},
		].forEach((pattern, key) => {
			try {
				this.fs.copyTpl(
					this.templatePath(pattern.from),
					pattern.to ? this.destination(pattern.to) : this.destination(),
					this.options,
				)
			} catch (e) {
				// console.log(key)
				// console.log(e)
			}
		})

		// All dote files.
		return this.commitWriteFiles();
	}
}
