const path = require('path')
const glob = require('glob')
const QuestionGenerator = require('../io/QuestionGenerator')
const {sortByDependencies, getGeneratorsAndDependencies} = require('../dependencies/dependencies')
const AppPath = require('./AppPath');
const {slug} = require('../io/io')

module.exports = class extends QuestionGenerator {

	constructor(args, options) {
		options.fromApp = true
		super(args, options)

		// Sauvegarde des options.
		this.options = options

		// Récupération du namespace.
		this.namespace = this.options.namespace.split(':').filter(item => item !== 'app').join(':')
	}

	/**
	 * Lance les générateurs sélectionner.
	 */
	async main() {
		// Check le contexte. Si on est dans un dossier préparé, on sort.
		this.checkContext();

		// Initialisation des options.
		const optionsData = await this.defineOptions();

		// Initialisation du workDir
		this.initWorkDir(optionsData.options);

		// Lancement de chaque generator.
		optionsData.selectedGenerators.forEach(generator => {
			this.composeWith(`${this.namespace}:${generator.name}`, {options: optionsData.options, y: true});
		})
	}

	/**
	 * Retourne le chemin du répertoire des générateurs.
	 *
	 * @return {*}
	 * @private
	 */
	_getGeneratorsRepPath() {
		return path.join(this._environmentOptions.resolved, '../../');
	}

	/**
	 * Retourne la liste de tous les générateurs.
	 * @private
	 */
	_getAllSubGenerators() {
		// Initialisation du
		return sortByDependencies(glob.sync(this._getGeneratorsRepPath() + '/*/index.js')
			// On formate le tout.
			.map(generatorPath => {
				return {
					'name': path.basename(path.dirname(generatorPath)),
					'path': generatorPath
				}
			})
			// Filtre pour éviter le générateur courant.
			.filter(generatorPath => {
				return generatorPath.name.toLowerCase() !== 'app';
			}))
	}

	/**
	 * Retourne la liste des generateurs sélectionnés par l'utilisateur
	 *
	 * @return {Promise<{path: string, name: *}[]>}
	 * @private
	 */
	async _getSelectedGenerators() {
		// Récupération de toutes les sous-taches.
		const generators = this._getAllSubGenerators()

		if (this.silent) {
			return generators;
		}

		// Prompt sur celles à utiliser.
		let selectedGenerators = await this.prompt([
			{
				type: "checkbox",
				name: "selection",
				messages: "Quelles actions doivent-être faites?",
				choices: generators.map(item => {
					item.checked = true
					return item;
				})
			}
		])
		return getGeneratorsAndDependencies(
			generators.filter(generator => selectedGenerators.selection.indexOf(generator.name) > -1),
			generators
		);
	}

	/**
	 *
	 * @param selectedGenerators
	 * @private
	 */
	async _askQuestionsOfSelectedGenerators(selectedGenerators) {
		let options = this.options,
			keys = [];

		// Parse generators.
		for (const generator of selectedGenerators) {
			const optionsData = await this._askQuestionOfGenerator(generator, options)
			options = {...options, ...optionsData.values}
			keys = keys.concat(optionsData.keys.filter(key => keys.indexOf(key) < 0))
		}

		// Clean options.
		return this.cleanOptions(options, keys)
	}

	/**
	 * Génère les options en fonction d'un seul generator
	 * @param generator
	 * @param options
	 * @return {Promise<{keys: [], values: {}}>}
	 * @private
	 */
	async _askQuestionOfGenerator(generator, options) {
		let generatorClass = require(generator.path),
			values = {},
			keys = [];

		const generatorInstance = new generatorClass({}, {options: options, y: true})

		if (typeof generatorInstance.askQuestions === 'function') {
			values = {...options, ...(await generatorInstance.askQuestions(options))}
		}

		// Récupération des clés d'options
		if (typeof generatorInstance.getQuestions === 'function') {
			keys = generatorInstance.getQuestions().map(item => item.name)
		}

		return {
			keys: keys,
			values: values
		};
	}

	/**
	 * Définit les options en fonction des sélections de l'utilisateur.
	 *
	 * @return {Promise<{options: *, selectedGenerators: *}|*>}
	 */
	async defineOptions() {
		// Récupération des generator à executer
		const selectedGenerators = await this._getSelectedGenerators()

		this.options = {...this.options, ...await this.workDirQuestion()};

		// Prompt data.
		const options = await this._askQuestionsOfSelectedGenerators(selectedGenerators);

		// Résumé des actions.
		console.log("===================")
		console.log("Generators")
		console.table(selectedGenerators, ['action']);

		// Résumé des options .
		console.log("===================")
		console.log("Options")
		const displayOptions = {}
		// hide secrests
		Object.keys(options).map(key => {
			displayOptions[key] = options[key];
			if(key.indexOf('secret') === 0 ){
				displayOptions[key] = (new Array(options[key].length)).fill('*').join('')
			}
		})
		console.table(displayOptions);

		if (!this.silent) {
			const confirm = await this.prompt([
				{
					type: 'confirm',
					name: 'confirm',
					message: 'Valider et lancer le processus ?',
				}
			]);

			// En cas de non confirmation on recommence.
			if (!confirm.confirm) {
				return this.defineOptions()
			}
		}

		return {
			selectedGenerators: selectedGenerators,
			options: options
		};
	}

	/**
	 * Vérifie le contexte de travail.
	 * Throw une erreur si le répertoire de travail existe dans l'arbo.
	 */
	checkContext() {
		if (AppPath.getWorkDir(this.destinationPath())) {
			throw 'Le répertoire de travail est déjà créé. Vous ne pouvez pas lancer le générateur principal'
		}
	}

	/**
	 * Initialisation du répertoire de travail
	 */
	async workDirQuestion() {
		return this.askQuestions(this.options)
	}

	/**
	 * @return {{filter: (function(*=): *), name: string, type: string, message: string, required: boolean}[]}
	 */
	getQuestions() {
		return [
			{
				type: "input",
				name: "projectId",
				message: "ID du projet (nom du répertoire, slug)",
				required: true,
				filter: (v) => {
					return slug(v)
				}
			},
		]
	}

	/**
	 * Crée le répertoire de travail
	 * @param options
	 */
	initWorkDir(options = {}) {
		const values = {};
		Object.keys(options).forEach(( key) => {
			if( key.indexOf('secret') !== 0 ){
				values[key] = options[key];
			}
		});

		this.fs.copyTpl(
			this.templatePath('./**/.*'),
			this.destinationPath(options.projectId),
			{
				data: JSON.stringify(values)
			}
		)

		this.commitWriteFiles();
	}
}
