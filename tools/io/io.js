const slugify = require('slugify')

/**
 * Retourne uniquement la liste des questions dont les réponses
 * ne sont pas déjà présentes dans data.
 *
 * @param questions
 * @param data
 */
module.exports.getNeededQuestions = function (questions, data) {
    return questions.filter(question => {
        return typeof data[question['name']] === "undefined";
    })
}

/**
 * Retourne les options en fonction des arguments et opts.
 *
 * @param args
 * @param opts
 */
module.exports.getOptionsFromArgs = function (args, opts) {
    let options = opts

    if (typeof options.options !== "undefined" ) {
        options = {...options, ...options.options}
    }

    return options;
}

module.exports.slug = function(args){
    return slugify(args, {
        lower: true,
        replacement:'_',
    })
}
