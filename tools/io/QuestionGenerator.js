const Generator = require('yeoman-generator')
const {getOptionsFromArgs, getNeededQuestions} = require('./io');
const AppPath = require('../app/AppPath')
const fs = require('fs')
const path = require('path')

module.exports = class extends Generator {

    constructor(args, opts) {
        super(args, opts)

        // Pas de question si valeur par défaut.
        this.silent = opts ? opts.y : false

        // Définition des optinos
        this.options = this._getOptions(args, opts)
    }

    /**
     * Retourne la liste des questions à "prompter"
     * @return {*[]}
     * @private
     */
    getQuestions() {
        return this._getQuestions();
    }

    /**
     * Abstract - Retourne la liste des questions à "prompter"
     * @return {*[]}
     * @private
     */
    _getQuestions() {
        return [];
    }

    /**
     * Prompt sur la liste des questions définies dans _getQuestions si nécessaire.
     * @return {*}
     * @private
     */
    async askQuestions(options) {
        if (this.silent) {
            const questions = getNeededQuestions(this._getQuestions(), options)

            if (questions.length > 0) {
                options = {...this.options, ...await this.prompt(questions)};
            }
        }
        else {
            options = await this.prompt(this.getQuestions().map(question => {
                if (typeof options[question.name] !== "undefined") {
                    question.default = options[question.name]
                }
                return question;
            }))
        }

        return options;
    }

    /**
     * Initialise les options en fonction des arguments de commande et des opts passés.
     *
     * @private
     */
    _getOptions(args, opts = {}) {
        // On récupère les données du .yo-drupal
        let options = this.getOptionsFromFile();

        options = {
            ...options,
            ...getOptionsFromArgs(args, opts)
        };

        // Si le generator n'est pas appelé en dépendance de l'app général,
        // on ne filtre les options pour ne garder que le nécessaire.
        if (options.fromApp !== true) {
            options = this.cleanOptions(options, this.getQuestions().map(item => item.name))
        }

        return options
    }

    /**
     * Retourne les options issues uniquement des questions.
     *
     * @param options
     * @param keys
     * @return {{}}
     */
    cleanOptions(options, keys) {
        const cleanOptions = {}
        Object.values(keys).forEach(key => {
            cleanOptions[key] = options[key]
        })
        return cleanOptions
    }

    /**
     * Commit write files.
     * @return {Promise<unknown>}
     */
    commitWriteFiles(){
        return new Promise((resolve)=>{
            this._writeFiles(resolve);
        })
    }

    /**
     * REtourne les options définies dans le .yo-drupal
     * @return {{}}
     */
    getOptionsFromFile(){
        const workdir = AppPath.getWorkDir(this.destinationPath())
        let options = {}
        if( workdir ){
            try{
                options = JSON.parse(fs.readFileSync(path.join(workdir,AppPath._yoFileName), 'utf8'));
            }
            catch (e){
                // do nothing.
            }
        }

        return options
    }
}
