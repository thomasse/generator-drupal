# Drupal Générateur

## Installation
1. Installez yeoman en global :   
  `npm i -g yo@3.1.1`
2. Indiquez a npm que c'est un repo yo  
  `npm link`
3. Rendez-vous ensutie dans le répertoire qui va contenir votre projet drupal
`cd {ce que vous voulez mais hors du projet courant}`
4. Lancer yo drupal et se laisser guider.
`yo drupal`
